package swexample.test;

import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
	public static void main(String[] args) {
	     
		Result result = JUnitCore.runClasses(RingBufferTest.class);

	      PrintLog("Total test count:", String.valueOf(result.getRunCount()));
	     
	      for (Failure failure : result.getFailures()) { 
	         PrintLog("Failure:", failure.toString());
	      }
	     
	      PrintLog("Test Result: ", String.valueOf(result.wasSuccessful()));
	     
	   }
	
	
	public static void PrintLog(String title, String message)
	{
		System.out.println("***** "+title + message+" *****\n");
	}
	
	
}
