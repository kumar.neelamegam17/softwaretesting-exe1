package swexample.test;

public class RingBufferException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RingBufferException(String msg) {
		super(msg);
	}

}
