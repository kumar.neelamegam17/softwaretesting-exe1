package swexample.test;


import static org.junit.Assert.*;

import java.util.Iterator;
import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import swexample.RingBuffer;

public class RingBufferTest {

	private RingBuffer<Integer> ringBuffer;
	private Iterator<Integer> iterator;
	 
	@Before
	public void initiateTest() {
		ringBuffer = new RingBuffer<>(3);
		iterator = ringBuffer.iterator();
	}
	
	@After
	public void endTest() {
		ringBuffer = null;
		iterator = null;
	}
	
	@Test
	public void testEnqueue() throws RingBufferException {
		ringBuffer.enqueue(0);		
		ringBuffer.enqueue(1);
		ringBuffer.enqueue(2);
		assertEquals(ringBuffer.size(), 3);
	}
	
	@Test
	public void testDequeue() throws RingBufferException {
		ringBuffer.enqueue(0);
		ringBuffer.enqueue(1);
		ringBuffer.enqueue(2);
		assertEquals(ringBuffer.dequeue(), new Integer(0));
		assertEquals(ringBuffer.dequeue(), new Integer(1));
		assertEquals(ringBuffer.dequeue(), new Integer(2));
	}
	
	@Test
	public void testEnqueueOverflow() throws RingBufferException {
		ringBuffer.enqueue(0);
		ringBuffer.enqueue(1);
		ringBuffer.enqueue(2);
		// exception must occur exactly in this call!
		ringBuffer.enqueue(3);
		fail("RingBuffer does not report overflow!");
	}
	 
	@Test(expected = RingBufferException.class)
	public void testDequeueUnderflow() throws RingBufferException {
		ringBuffer.dequeue();
	}
	
	@Test
	public void testEnqueueCircular() throws RingBufferException {
		ringBuffer.enqueue(0);
		ringBuffer.enqueue(1);
		ringBuffer.enqueue(2);
		ringBuffer.dequeue();
		ringBuffer.dequeue();
		ringBuffer.enqueue(3);
		ringBuffer.enqueue(4);
		assertEquals(ringBuffer.size(), 3);
		assertEquals(ringBuffer.dequeue(), new Integer(2));
		assertEquals(ringBuffer.dequeue(), new Integer(3));
		assertEquals(ringBuffer.dequeue(), new Integer(4));
		assertEquals(ringBuffer.size(), 0);
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testIteratorOfEmptyRingBuffer() {
		assertFalse(iterator.hasNext());
		iterator.next();
	}
	
	@Test
	public void testIteratorContainsAll() throws RingBufferException {
		ringBuffer.enqueue(0);
		ringBuffer.enqueue(1);
		ringBuffer.enqueue(2);
		ringBuffer.dequeue();
		ringBuffer.dequeue();
		ringBuffer.enqueue(0);
		ringBuffer.enqueue(1);
		// (2, 0, 1)
		
		// assuming that iterator must not return elements in a specific order
		boolean item1 = false, item2 = false, item3 = false;
		for(int i = 0; i < 3; i++) {
			switch(iterator.next()) {
			case 0:
				item1 = true;
				break;
			case 1:
				item2 = true;
				break;
			case 2:
				item3 = true;
				break;
			default:
				fail("Iterator returned item that was never enqueued!");
				break;
			}
		}
		
		assertTrue(item1);
		assertTrue(item2);
		assertTrue(item3);
		assertFalse(iterator.hasNext());
	}
	
}
