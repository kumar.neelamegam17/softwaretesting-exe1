package assignment01;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import static org.junit.Assert.*;


public class RingBufferTest {
RingBuffer<String> ringbuffer;
@Before
public void createRingBufferStringsOfCapacity4() {
ringbuffer = new RingBuffer<>(4);
}


@Test //test if the buffer is empty
public void testEmptynessOfNewRingBuffer() {
assert(true,ringbuffer.isEmpty());

}

@Test //test if the buffer is full or not
public void testEmptynessOfNewRingBuffer() {
ringbuffer.enqueue("abc");
ringbuffer.enqueue("def");
ringbuffer.enqueue("ghi");
ringbuffer.enqueue("jkl");
assert(true,ringbuffer.iisFull());

}

@Test //test the size of the buffer
public void testEnqueue() throws RingBufferException {
ringbuffer.enqueue("abc");
ringbuffer.enqueue("def");
ringbuffer.enqueue("ghi");
assertEquals(3, ringbuffer.size());
}

@Test //test what is expected as output
public void testDequeue() throws RingBufferException {
ringbuffer.enqueue("abc");
ringbuffer.enqueue("def");
ringbuffer.enqueue("ghi");
ringbuffer.dequeue();
String s = ringbuffer.dequeue();
assertEquals("ghi", s);
}

@Before
@Rule
public ExpectedException thrown = ExpectedException.none();

@Test// test overflow of the buffer
    public void testExceptionThrownMessageOverflow() throws RingBufferException {
        thrown.expect(RingBufferException.class);
        thrown.expectMessage("overflow");

        ringbuffer.enqueue("abc");
        ringbuffer.enqueue("def");
        ringbuffer.enqueue("ghi");
        ringbuffer.enqueue("jkl");
		ringbuffer.enqueue("mno");

    }
	
 @Test // test underflow of the buffer
    public void testExceptionThrownMessageUnderflow() throws RingBufferException {
        thrown.expect(RingBufferException.class);
        thrown.expectMessage("underflow");

        ringbuffer.enqueue("abc");
		ringbuffer.enqueue("def");
        ringbuffer.dequeue();
        ringbuffer.dequeue();
    }

}
