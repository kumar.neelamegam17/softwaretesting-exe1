package assignment01;

import java.util.Observable;
import java.util.Observer;

public class DummyObserver implements Observer {

    private String message;
    private int numOfMessages = 0;

    public String getMessage() {
        return message;
    }

    public int getNumOfMessages() {
        return numOfMessages;
    }

   

}
