package assignment01;

import java.util.Observer;

public interface Subject {
    void AddObserver(Observer o);
    void RemoveObserver(Observer o);
    void NotifyObservers();
}